# *Movemos Project* - Projeto Movemos
*Visualization prototype of bus routes in Rio de Janeiro. All data comes from the open data portal of Rio de Janeiro State. Historic data was saved to a database for the project.*

Protótipo de visualização das rotas de ônibus no Rio de Janeiro. Todos os dados são provenientes do portal de dados abertos do estado do Rio de Janeiro. Um histórico foi armazenado em servidor próprio para utilização no projeto.


## Heatmap_rio.py
*Script that generates a heatmap of 3 million gps data points using datashader.*

Script gerador do heatmap com base em 3 milhões de pontos de gps utilizando datashader.


## heatmap_rio.png
*Heatmap image*

Imagem do heatmap.
![Alt text](https://bitbucket.org/bdore/osb-rio/raw/054f71626a78379776ee07100c7c4ed83f2b9d7f/heatmap_rio.png)

*The prototype is a personal exploration of another project where I collaborated: https://github.com/OSB-Rio/movemos*

O protótipo é uma exploração pessoal de outro projeto onde colaborei: https://github.com/OSB-Rio/movemos