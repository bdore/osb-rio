import os

from pyproj import Proj, transform

import datashader as ds
from datashader.colors import Hot
from datashader.utils import export_image
from datashader import transfer_functions as tf

import matplotlib.pyplot as plt
import pandas as pd
import osmnx as ox
import numpy as np

from peewee import *

plt.switch_backend('agg')


#ENVIRONMENT VARIABLES
DATABASE = os.environ['DATABASE']
USERNAME = os.environ['USERNAME']
PASSWORD = os.environ['PASSWORD']
HOST = os.environ['HOST']
PORT = int(os.environ['PORT'])

mysql_db = MySQLDatabase(DATABASE, user=USERNAME, password=PASSWORD, host=HOST, port=PORT)
mysql_db.connect()
query = mysql_db.execute_sql('SELECT * FROM posicoes LIMIT 3000000')
mysql_db.close()

data_list = list()

for item in query:
    registro = {'timestamp': item[0],
                'ordem': item[1],
                'linha': item[2],
                'lat': item[3],
                'long': item[4],
                'velocidade': item[5]}
    data_list.append(registro)

gps_data = pd.DataFrame(data_list)
columns = ['timestamp', 'ordem', 'linha', 'lat', 'long', 'velocidade']
gps_data = gps_data[columns]

#CONSULTA BBOX DO LOCAL
location_gdf = ox.gdf_from_place('Rio de Janeiro, Rio de Janeiro, Rio de Janeiro, Brazil')
location_latlong = {'lat': location_gdf[['bbox_north', 'bbox_south']].values[0],
                  'long': location_gdf[['bbox_east', 'bbox_west']].values[0]}

# RESTRINGE OS PONTOS AO LOCAL
for item in ['lat', 'long']:
    gps_data = gps_data[gps_data[item].apply(lambda x: min(location_latlong[item]) <= x <= max(location_latlong[item]))]

# IDENTIFICA E REMOVE OUTLIERS
data_mean, data_std = np.mean(gps_data['lat']), np.std(gps_data['lat'])
cut_off = data_std * 3
lower, upper = data_mean - cut_off, data_mean + cut_off
gps_data = gps_data[gps_data['lat'].apply(lambda x : lower < x < upper)]

#output_notebook()

#gps_data = pd.read_csv('409_clean.csv')

input_proj = Proj(init='epsg:4326')
output_proj = Proj(init='epsg:3857')

lat = gps_data['lat'].values
long = gps_data['long'].values

projected_lat = list()
projected_long = list()

for item in zip(lat, long):
    x, y = transform(input_proj, output_proj, item[1], item[0])
    projected_long.append(x)
    projected_lat.append(y)

gps_data['long'] = projected_long
gps_data['lat'] = projected_lat

gps_data['signals'] = 1

bound = 20026376.39
bounds = dict(x_range = (location_latlong['long'][1], location_latlong['long'][0]),
              y_range = (location_latlong['lat'][1], location_latlong['lat'][0]))

x_range = list()
y_range = list()

for item in zip(bounds['x_range'], bounds['y_range']):
    x, y = transform(input_proj, output_proj, item[0], item[1])
    x_range.append(x)
    y_range.append(y)

plot_width  = int(3000)
plot_height = int(1500)

def create_image(x_range, y_range, w=plot_width, h=plot_height):
    cvs = ds.Canvas(plot_width=w, plot_height=h, x_range=x_range, y_range=y_range)
    agg = cvs.points(gps_data, 'long', 'lat',  ds.count('signals'))
    img = tf.shade(agg, cmap=Hot, how='eq_hist')
    return tf.dynspread(img, threshold=0.5, max_px=4)


img = create_image(x_range, y_range)
export_image(img, filename='test6', fmt='.png', background = "black")